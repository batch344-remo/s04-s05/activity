// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "my home in Quezon City");

        Contact contact2 = new Contact("Jane Doe", "+639162148573", "My home in Caloocan City");

        // System.out.println(contact2.getName());
    }
}