public class Contact {
    // Properties
    private String name;
    private String contactNumber;
    private String address;

    // Constructors
    public Contact() {}

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // Getters
    public String getName() {
        return this.name;
    }
    public String getContactNumber() {
        return contactNumber;
    }
    public String getAddress() {
        return address;
    }

    // Setters

    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
