import java.util.ArrayList;

public class Phonebook {
    // Property
    private ArrayList<String> contacts = new ArrayList<String>();

    // Constructors
    public Phonebook() {}

    public Phonebook(ArrayList<String> contacts) {
        this.contacts = contacts;
    }

    // Getters
    public ArrayList<String> getContacts() {
        return this.contacts;
    }

    // Setters
    public void setContacts(ArrayList<String> contacts) {
        this.contacts = contacts;
    }
}
